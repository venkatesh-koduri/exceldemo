package com.example.exceldemo;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelpoi {
	
	
	 @SuppressWarnings("resource")
	public static void main(String[] args) 
	    {
		 
		   StringBuilder sb = new StringBuilder();
		   Path filepath = Paths.get("src/main/resources/CalculateDepreciation.xlsx").toAbsolutePath();
	        try(FileInputStream file = new FileInputStream(new File(filepath.normalize().toString())))
	        {
	            
	            //Create Workbook instance holding reference to .xlsx file
	            XSSFWorkbook workbook = new XSSFWorkbook(file);
	 
	            //Get first/desired sheet from the workbook
	            XSSFSheet sheet = workbook.getSheetAt(0);
	 
	            Iterator<Row> rowIterator = sheet.iterator();
	            while (rowIterator.hasNext()) 
	            {
	                Row row = rowIterator.next();
	                //For each row, iterate through all the columns
	                Iterator<Cell> cellIterator = row.cellIterator();
	               
	                while (cellIterator.hasNext()) 
	                {
	                    Cell cell = cellIterator.next();
	                    //Check the cell type and format accordingly
	                    switch (cell.getCellType()) 
	                    {
	                        case NUMERIC:
	                            System.out.print(cell.getNumericCellValue() + "t");
	                            sb.append(cell.getNumericCellValue() + "| ");
	                            break;
	                        case STRING:
	                            System.out.print(cell.getStringCellValue() + "t");
	                            sb.append(cell.getStringCellValue() + "| ");
	                            break;
	                    }
	                }
	                sb.append("\n");
	            }
	                        
	            Path path = Paths.get("D:\\writedatasamp.txt");	            
	            Files.write(path, Arrays.asList(sb.toString()));
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	    }

}
