package com.example.exceldemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.monitorjbl.xlsx.StreamingReader;

public class ReadExcel {
	
	 public static void main(String[] args) {
		 StringBuilder sb = new StringBuilder();
		 Path filepath = Paths.get("src/main/resources/CalculateDepreciation.xlsx").toAbsolutePath();
		 System.out.println(filepath.normalize().toString());
		 try ( InputStream is = new FileInputStream(new File(filepath.normalize().toString()));
			   Workbook workbook = StreamingReader.builder()
						          .rowCacheSize(100)
						          .bufferSize(4096)
						          .open(is)) {
			  for (Sheet sheet : workbook){
			    for (Row r : sheet) {
			      for (Cell c : r) {	        
			        sb.append(c.getStringCellValue() + "| ");
			      }
			      sb.append("\n");
			    }
			  }
			  Path path = Paths.get("D:\\writedatausingstreamreader.txt");	            
	          Files.write(path, Arrays.asList(sb.toString()));
		}catch (Exception e){
	        System.out.println(e);
	    }

	  }

}
